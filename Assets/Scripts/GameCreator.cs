using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameCreator : MonoBehaviour
{
    [Header("UI settings")]
    public UIManager uIManager;
    public WindowManager windowManager;
    public Settings settings;
    public Window playWindow, pauseWindow;

    [Header("Parameters settings")]
    public NumberPicker playerCountPicker;
    public NumberPicker AIPlayerCountPicker, fieldWidthPicker, fieldHeightPicker;

    [Header("Generated GOs parents settings")]
    public RectTransform gameParent;
    public Transform playersParent;

    [Header("Prefabs settings")]
    public GameData gameDataPrefab;
    public GameManager gameManagerPrefab;
    public HumanPlayer humanPlayerPrefab;
    public AIPlayer AIPlayerPrefab;

    [Header("Other settings")]
    public float gameEndDelay;
    public BaseResourceLoader loader;
    public Utils.Event<GameData> onGameCreated = new Utils.Event<GameData>();

    private GameData gameData;
    private GameManager gameManager;

    private IList<string> names;
    private int playerCount, AICount;

    private void Start() 
    {
        playerCountPicker.onValueChanged.AddListener(v => AIPlayerCountPicker.maxValue = v);
    }

    public void CreateGame() 
    {
        var AIPlayerCount = AIPlayerCountPicker.value;
        var humanPlayerCount = playerCountPicker.value - AIPlayerCount;

        var names = new List<string>();
        var firstName = PlayerPrefs.GetString(Settings.PLAYER_NAME_PREFS, "");
        if(firstName != "") {
            names.Add(firstName);
        }
        GetPlayerName(names, humanPlayerCount - names.Count, () => {
            this.names = names;
            playerCount = humanPlayerCount;
            AICount = AIPlayerCount;
            InitializeGame(names, humanPlayerCount, AIPlayerCount);
        });
    }

    private void GetPlayerName(IList<string> namesContainer, int count, Action onAllNamesGiven) 
    {
        if(count <= 0) {
            onAllNamesGiven();
            return;
        }
        
        windowManager.CreateDialog<TextInputDialog>($"Enter player's №{namesContainer.Count + 1} name", "Enter name here...", d => {
            if(d.inputField.text.Length > 0) {
                if(namesContainer.Count == 0) {
                    settings.playerName = d.inputField.text;
                }
                namesContainer.Add(d.inputField.text);
                GetPlayerName(namesContainer, count - 1, onAllNamesGiven);
            }
        });
    }

    private void InitializeGame(IList<string> playerNames, int playerCount, int AICount) 
    {
        if(gameData || gameManager) return;

        gameData = Instantiate(gameDataPrefab, gameParent);
        gameManager = Instantiate(gameManagerPrefab);

        gameData.field.gameManager = gameManager;
        gameData.field.size = new Vector2Int(fieldWidthPicker.value, fieldHeightPicker.value);

        gameData.field.onFieldCreated.AddListener(gameData.fieldView.InitializeView);
        gameData.field.onFieldChanged.AddListener(gameData.fieldView.ShowMark);
        gameData.fieldInspector.onWinLineFound.AddListener(gameData.fieldView.ShowWinLine);

        for(int i = 0; i < playerCount; i++) {
            var player = Instantiate(humanPlayerPrefab, playersParent);
            player.playerName = playerNames[i];
            gameManager.players.Add(player);
            player.field = gameData.field;
            player.fieldView = gameData.fieldView;
            gameData.fieldButton.onClick.AddListener(player.SetCellOnClick);
        }

        for(int i = 0; i < AICount; i++) {
            var player = Instantiate(AIPlayerPrefab, playersParent);
            player.playerName = $"Bot {i + 1}";
            gameManager.players.Add(player);
            gameManager.onTurnChanged.AddListener(player.MakeMove);
            player.field = gameData.field;
            player.fieldInspector = gameData.fieldInspector;
        }

        gameManager.onPlayerWon.AddListener(p => {
            StartCoroutine(
                Utils.DelayedCall(gameEndDelay, () => 
                    windowManager.CreateDialog<AlertDialog>(
                        "Congratulations!", 
                        $"Player {p.playerName} won!", 
                        d => ClearGoToMenu()
                    )
                )
            );
        });

        gameData.fieldInspector.onDraw.AddListener(() => {
            StartCoroutine(
                Utils.DelayedCall(gameEndDelay, () => 
                    windowManager.CreateDialog<AlertDialog>(
                        "Draw!", 
                        $"Nobody won!", 
                        d => ClearGoToMenu()
                    )
                )
            );
        });

        loader.onLoad.AddListener(OnLoadedGame);
        windowManager.CloseWindow(playWindow.windowName);
        loader.LoadResources();
    }

    private void OnLoadedGame(bool isLoading) 
    {
        if(!isLoading) {
            uIManager.ActivateState("Game");
            loader.onLoad.RemoveListener(OnLoadedGame);
            onGameCreated.Invoke(gameData);
            gameData.field.gameManager.StartGame();
        }
    }
    
    private void OnLoadedMenu(bool isLoading) 
    {
        if(!isLoading) {
            uIManager.ActivateState("Menu");
            loader.onLoad.RemoveListener(OnLoadedMenu);
        }
    }

    public void QuitLevel() 
    {
        windowManager.CreateDialog<QuestionDialog>("Are sure to quit?", "Game data will NOT be saved.", d => ClearGoToMenu());
    }

    public void RestartLevel() 
    {
        windowManager.CreateDialog<QuestionDialog>("Are sure to restart?", "Game data will NOT be saved.", d => {
            Clear();
            InitializeGame(names, playerCount, AICount);
        });
    }

    private void Clear() 
    {
        if(!gameData || !gameManager) return;

        Destroy(gameData.gameObject);
        Destroy(gameManager.gameObject);
        playersParent.DestroyAllChildren();
        gameData = null;
        gameManager = null;

        windowManager.CloseWindow(pauseWindow.windowName);
    }

    private void ClearGoToMenu() 
    {
        Clear();
        loader.onLoad.AddListener(OnLoadedMenu);
        loader.LoadResources();
    }
}
