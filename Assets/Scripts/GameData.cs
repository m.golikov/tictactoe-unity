using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameData : MonoBehaviour
{
    public TicTacToeField field;
    public TicTacToeFieldView fieldView;
    public BaseTicTacToeFieldInspector fieldInspector;
    public Button fieldButton;
}
