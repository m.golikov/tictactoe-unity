using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public List<BasePlayer> players;
    public Utils.Event<BasePlayer> onTurnChanged = new Utils.Event<BasePlayer>(), onPlayerWon = new Utils.Event<BasePlayer>();

    private bool _gameIsEnded;
    public bool gameIsEnded 
    {
        get => _gameIsEnded;
        set {
            _gameIsEnded = value;
            onPlayerWon.Invoke(players[currentPlayerIndex]);
        }
    }
    private int _currentPlayerIndex;
    public int currentPlayerIndex 
    {
        get => _currentPlayerIndex;
        set {
            _currentPlayerIndex = GetPlayerIndexByDelta(value - currentPlayerIndex);
            onTurnChanged.Invoke(players[currentPlayerIndex]);
        }
    }
    public BasePlayer currentPlayer => players[currentPlayerIndex];

    public void StartGame() 
    {
        players.Shuffle();
        currentPlayerIndex = 0;
    }

    public int GetPlayerIndexByDelta(int delta) 
    {
        var cycle = (currentPlayerIndex + delta) / players.Count;
        return ((-cycle + 1) * players.Count + currentPlayerIndex + delta) % players.Count;
    }
}
