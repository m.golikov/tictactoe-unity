using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BasePlayer : MonoBehaviour
{
    public string playerName;
    public TicTacToeField field;

    public abstract Vector2Int GetChosenCellCoords();

    public void SetCell(Vector2Int coords) 
    {
        field.SetCell(coords, this);
    }
}
