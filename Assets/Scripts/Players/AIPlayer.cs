using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPlayer : BasePlayer
{
    public BaseTicTacToeFieldInspector fieldInspector;
    public float moveTime;

    public void MakeMove(BasePlayer player) 
    {
        if(player == this) {
            StartCoroutine(DelayedMove(() => {
                SetCell(GetChosenCellCoords());
            }));
        }
    }

    public override Vector2Int GetChosenCellCoords()
    {
        var requiredLength = fieldInspector.GetRequiredLength();

        Vector2Int result = CheckPlayer(this, requiredLength);
        if(result != TicTacToeField.INVALID_COORDS) return result;

        foreach(var player in field.gameManager.players) {
            if(player == this) continue;
            result = CheckPlayer(player, requiredLength);
            if(result != TicTacToeField.INVALID_COORDS) return result;
        }

        return GetRandomCellCoords();
    }

    private Vector2Int CheckPlayer(BasePlayer player, int requiredLength) 
    {
        var index = field.gameManager.players.IndexOf(player) + 1;
        Vector2Int result;

        result = CheckDirectLines(requiredLength, Vector2Int.right, index);
        if(result != TicTacToeField.INVALID_COORDS) return result;
        result = CheckDirectLines(requiredLength, Vector2Int.up, index);
        if(result != TicTacToeField.INVALID_COORDS) return result;
        result = CheckDiagonalLines(requiredLength, Vector2Int.one, index);
        if(result != TicTacToeField.INVALID_COORDS) return result;
        result = CheckDiagonalLines(requiredLength, new Vector2Int(1, -1), index);
        if(result != TicTacToeField.INVALID_COORDS) return result;

        return TicTacToeField.INVALID_COORDS;
    }

    private Vector2Int CheckDiagonalLines(int requiredLength, Vector2Int direction, int cellType) 
    {
        var start = new Vector2Int(direction.x > 0 ? 0 : (field.size.x - 1), direction.y > 0 ? 0 : (field.size.y - 1));
        var end = new Vector2Int(direction.x > 0 ? field.size.x + 1 - requiredLength : requiredLength - 2, 
                direction.y > 0 ? field.size.y + 1 - requiredLength : requiredLength - 2);
        for(int i = start.x; i * direction.x < end.x * direction.x; i += direction.x) {
            for(int j = start.y; j * direction.y < end.y * direction.y; j += direction.y) {
                var count = 0;
                Vector2Int aim = TicTacToeField.INVALID_COORDS;
                
                var diagonalStart = new Vector2Int(i, j);
                for(int k = 0; k < requiredLength; k++) {
                    var coords = diagonalStart + k * direction;
                    if(field.GetCell(coords) == cellType) {
                        count++;
                    } else if(field.GetCell(coords) == 0) {
                        aim = coords;
                    }
                }

                if(count == requiredLength - 1 && aim != TicTacToeField.INVALID_COORDS) {
                    return aim;
                }
            }
        }

        return TicTacToeField.INVALID_COORDS;
    }

    private Vector2Int CheckDirectLines(int requiredLength, Vector2Int direction, int cellType) 
    {
        var otherDirection = Vector2Int.one - direction;
        for(int i = 0; i <= Utils.Dot(field.size, direction) - requiredLength; i++) {
            for(int j = 0; j < Utils.Dot(field.size, otherDirection); j++) {
                var count = 0;
                Vector2Int aim = TicTacToeField.INVALID_COORDS;
                
                for(int k = 0; k < requiredLength; k++) {
                    var coords = (i + k) * direction + j * otherDirection;
                    if(field.GetCell(coords) == cellType) {
                        count++;
                    } else if(field.GetCell(coords) == 0) {
                        aim = coords;
                    }
                }

                if(count == requiredLength - 1 && aim != TicTacToeField.INVALID_COORDS) {
                    return aim;
                }
            }
        }

        return TicTacToeField.INVALID_COORDS;
    }

    private Vector2Int GetRandomCellCoords() 
    {
        var emptyCells = new Dictionary<Vector2Int, int>();

        for(int i = 0; i < field.size.x; i++) {
            for(int j = 0; j < field.size.y; j++) {
                var coords = new Vector2Int(i, j);
                var cell = field.GetCell(coords);
                if(cell == 0) {
                    emptyCells.Add(coords, cell);
                }
            }
        }

        if(emptyCells.Count > 0) {
            return new List<Vector2Int>(emptyCells.Keys)[UnityEngine.Random.Range(0, emptyCells.Count)];
        }

        return TicTacToeField.INVALID_COORDS;
    }

    private IEnumerator DelayedMove(Action action) {
        yield return new WaitForSeconds(moveTime);
        action();
    }
}
