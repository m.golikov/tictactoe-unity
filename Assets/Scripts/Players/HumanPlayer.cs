using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanPlayer : BasePlayer
{
    public TicTacToeFieldView fieldView;

    public void SetCellOnClick() 
    {
        SetCell(GetChosenCellCoords());
    }

    public override Vector2Int GetChosenCellCoords()
    {
        Vector2 markPosition = new Vector2();
        if(RectTransformUtility.ScreenPointToLocalPointInRectangle(fieldView.fieldParent, Input.mousePosition, null, out markPosition)) {
            return fieldView.GetFieldCoords(markPosition);
        }

        return TicTacToeField.INVALID_COORDS;
    }
}
