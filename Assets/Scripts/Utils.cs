using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public static class Utils
{
    public static int Dot(Vector2Int a, Vector2Int b) 
    {
        return a.x * b.x + a.y * b.y;
    }

    public static void MapListToDictionary<T1, T2, T3>(IList<T3> list, IDictionary<T1, T2> dictionary, Func<T3, T1> mapKeyFunction, Func<T3, T2> mapValueFunction) 
    {
        foreach(var element in list) {
            dictionary.Add(mapKeyFunction(element), mapValueFunction(element));
        }
    }

    public static void DestroyAllChildren(this Transform transform) 
    {
        while(transform.childCount > 0) {
            GameObject.DestroyImmediate(transform.GetChild(0).gameObject);
        }
    }

    public static IEnumerator DelayedCall(float delay, Action action) 
    {
        yield return new WaitForSeconds(delay);
        action();
    }

    public static void PerformByValue<T, VT>(IList<T> inputList, Comparison<T> comparison,  Func<T, VT> getValue, Action<T> onSatisfyingValue, VT value) where VT : IComparable<VT>
    {
        var list = new List<T>(inputList);
        list.Sort(comparison);
        for(int i = 0; i < list.Count - 1; i++) {
            if(value.CompareTo(getValue(list[i])) >= 0 && value.CompareTo(getValue(list[i + 1])) < 0) {
                onSatisfyingValue.Invoke(list[i]);
                return;
            }
        }
        if(value.CompareTo(getValue(list.Last())) >= 0) {
            onSatisfyingValue.Invoke(list.Last());
        }
    }

    public static void Shuffle<T>(this IList<T> list)
    {
        RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
        int n = list.Count;
        while (n > 1)
        {
            byte[] box = new byte[1];
            do provider.GetBytes(box);
            while (!(box[0] < n * (Byte.MaxValue / n)));
            int k = (box[0] % n);
            n--;
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    [Serializable]
    public class Event<T> : UnityEvent<T> 
    {
        public Event() : base() {}
    }

    [Serializable]
    public class Event<T0, T1> : UnityEvent<T0, T1> 
    {
        public Event() : base() {}
    }

    [Serializable]
    public class Event<T0, T1, T2> : UnityEvent<T0, T1, T2> 
    {
        public Event() : base() {}
    }
}
