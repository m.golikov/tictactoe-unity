using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Settings : MonoBehaviour
{
    public static readonly string PLAYER_NAME_PREFS = "player", SOUND_VOLUME_PREFS = "sound", RATING_PREFS = "rating";

    public WindowManager windowManager;
    public Utils.Event<string> onPlayerNameChanged = new Utils.Event<string>();
    public Utils.Event<float> onSoundVolumeChanged = new Utils.Event<float>();
    public Utils.Event<string> onPlayerRatingChanged = new Utils.Event<string>();

    public int rating 
    {
        get => PlayerPrefs.GetInt(RATING_PREFS, 0);
        set {
            PlayerPrefs.SetInt(RATING_PREFS, value);
            UpdateChangeables();
        }
    }
    public string playerName 
    {
        get => PlayerPrefs.GetString(PLAYER_NAME_PREFS);
        set {
            PlayerPrefs.SetString(PLAYER_NAME_PREFS, value);
            UpdateChangeables();
        }
    }
    public float soundVolume 
    {
        get => PlayerPrefs.GetFloat(SOUND_VOLUME_PREFS, 1);
        set {
            PlayerPrefs.SetFloat(SOUND_VOLUME_PREFS, value);
            UpdateChangeables();
        }
    }

    private void Start() 
    {
        UpdateChangeables();
    }

    public void ClearAllData() 
    {
        PlayerPrefs.DeleteAll();
        UpdateChangeables();
    }

    public void ChangeName() 
    {
        var dialog = windowManager.CreateDialog<TextInputDialog>($"Enter your name", "Enter name here...", d => {
            if(d.inputField.text.Length > 0) {
                playerName = d.inputField.text;
            }
        });
        dialog.inputField.text = playerName;
    }

    private void UpdateChangeables() 
    {
        onPlayerNameChanged.Invoke($"Name: {PlayerPrefs.GetString(PLAYER_NAME_PREFS, "{no name yet!}")}");
        onSoundVolumeChanged.Invoke(soundVolume);
        onPlayerRatingChanged.Invoke($"Rating: {rating}");
    }
}
