using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class NumberPicker : MonoBehaviour
{
    [SerializeField] private int _minValue, _maxValue;
    public int minValue 
    {
        get => _minValue;
        set {
            _minValue = value;
            UpdateValue();
        }
    }
    public int maxValue 
    {
        get => _maxValue;
        set {
            _maxValue = value;
            UpdateValue();
        }
    }
    [SerializeField] private int _value;
    public int value 
    {
        get => _value;
        set {
            _value = Mathf.Clamp(value, minValue, maxValue);
            onValueChanged.Invoke(_value);
        }
    }
    public Utils.Event<int> onValueChanged = new Utils.Event<int>();
    [Header("UI settings")]
    public Text valueText;
    public Button plusButton, minusButton;

    public void Start() 
    {
        plusButton.onClick.AddListener(() => value++);
        minusButton.onClick.AddListener(() => value--);
        onValueChanged.AddListener(v => valueText.text = v.ToString());
        UpdateValue();
    }

    public void UpdateValue() 
    {
        value = _value;
    }
}
