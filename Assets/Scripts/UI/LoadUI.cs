﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadUI : MonoBehaviour
{
    public BaseResourceLoader loader;
    public ProgressBar progressBar;
    public Text messageText;

    private void Awake() 
    {
        loader.onLoadProgress.AddListener(progressBar.ShowProgress);
        loader.onLoadMessage.AddListener(m => messageText.text = m);
        loader.onLoad.AddListener(l => gameObject.SetActive(l));
    }
}
