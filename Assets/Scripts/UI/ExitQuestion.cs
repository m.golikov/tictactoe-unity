using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitQuestion : MonoBehaviour
{
    public WindowManager windowManager;
    public UIManager uIManager;
    public GameCreator gameCreator;
    public GameObject menuState, gameState;

    private void Start() 
    {
        windowManager.onEscape.AddListener(ShowExitDialog);
    }

    public void ShowExitDialog() 
    {
        if(uIManager.activeState == menuState) {
            var dialog = windowManager.CreateDialog<QuestionDialog>("Are you sure to exit?", "You may lose unsaved progress.", d => Application.Quit());
        } else if(uIManager.activeState == gameState) {
            gameCreator.QuitLevel();
        }
    }
}
