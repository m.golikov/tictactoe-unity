﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundVolumeUI : ProgressBar
{
    [Serializable]
    public struct SoundImageData 
    {
        public Sprite sprite;
        public float volume;
    }

    public List<SoundImageData> imageDatas;
    public Image soundVolumeImage;

    public override void ShowProgress(float progress)
    {
        base.ShowProgress(progress);
        Utils.PerformByValue(imageDatas, (a, b) => (int)(b.volume - a.volume), l => l.volume, l => soundVolumeImage.sprite = l.sprite, progress);
    }
}
