using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public abstract class BaseDialogWindow : BaseWindow
{
    public Text titleText, messageText;
    public Button actionButton;
    public UnityEvent onAction = new UnityEvent();

    public virtual void Initialize<T>(string title, string message, Action<T> action) where T : BaseDialogWindow
    {
        onAction.AddListener(() => action(this as T));
        onAction.AddListener(Close);
        actionButton.onClick.AddListener(onAction.Invoke);

        titleText.text = title;
        messageText.text = message;
    }

    public override void Close()
    {
        base.Close();
        Destroy(gameObject);
    }

    public override void Open()
    {
        base.Open();
        gameObject.SetActive(true);
    }
}
