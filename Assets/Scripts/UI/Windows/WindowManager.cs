using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WindowManager : MonoBehaviour
{
    [SerializeField] private List<BaseWindow> premadeWindows;
    [SerializeField] private List<BaseDialogWindow> dialogWindowDatas;
    public RectTransform dialogWindowsParent;
    public UnityEvent onEscape;

    public BaseWindow currentWindow 
    {
        get {
            if(windowsStack.Count > 0) {
                return windowsStack.Last();
            }
            return null;
        }
    }

    private IDictionary<Type, BaseDialogWindow> dialogWindowPrefabs = new Dictionary<Type, BaseDialogWindow>();
    private IDictionary<string, BaseWindow> windows = new Dictionary<string, BaseWindow>();
    private IList<BaseWindow> windowsStack = new List<BaseWindow>();

    private void Start() 
    {
        Utils.MapListToDictionary(premadeWindows, windows, w => w.windowName, w => {
            w.Close();
            InitializeWindow(w);
            return w;
        });
        Utils.MapListToDictionary(dialogWindowDatas, dialogWindowPrefabs, d => d.GetType(), d => d);
    }

    private void Update() 
    {
        if(Input.GetKeyDown(KeyCode.Escape)) {
            if(windowsStack.Count > 0) {
                windowsStack.Last().Close();
            } else {
                onEscape.Invoke();
            }
        }
    }

    public void AddWindow(BaseWindow window) 
    {
        windows.Add(window.windowName, window);
        InitializeWindow(window);
    }

    public void OpenWindow(string name) 
    {
        BaseWindow window;
        if(windows.TryGetValue(name, out window) && !windowsStack.Contains(window)) {
            windowsStack.Add(window);
            window.Open();
        }
    }

    public void CloseWindow(string name) 
    {
        BaseWindow window;
        if(windows.TryGetValue(name, out window) && windowsStack.Contains(window)) {
            window.Close();
        }
    }

    public T CreateDialog<T>(string title, string message, Action<T> action) where T : BaseDialogWindow
    {
        BaseDialogWindow prefab;
        if(dialogWindowPrefabs.TryGetValue(typeof(T), out prefab) && typeof(T).IsInstanceOfType(prefab)) {
            var dialog = Instantiate(prefab, dialogWindowsParent) as T;
            InitializeWindow(dialog);
            dialog.Initialize<T>(title, message, action);
            dialog.Open();
            windowsStack.Add(dialog);
            return dialog;
        }
        return null;
    }

    private void InitializeWindow(BaseWindow window) 
    {
        window.windowManager = this;
        window.onClose.AddListener(() => windowsStack.Remove(window));
    }
}
