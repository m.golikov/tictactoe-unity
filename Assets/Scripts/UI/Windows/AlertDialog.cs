using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlertDialog : BaseDialogWindow
{
    public Image alertImage;

    public override void Initialize<T>(string title, string message, Action<T> action) 
    {
        base.Initialize(title, message, action);
        actionButton.onClick.AddListener(Close);
    }
}
