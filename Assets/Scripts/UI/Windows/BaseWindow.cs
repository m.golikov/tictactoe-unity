using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class BaseWindow : MonoBehaviour
{
    public string windowName;
    public WindowManager windowManager;
    public UnityEvent onOpen = new UnityEvent(), onClose = new UnityEvent();
    
    public virtual void Open() 
    {
        onOpen.Invoke();
    }

    public virtual void Close() 
    {
        onClose.Invoke();
    }
}
