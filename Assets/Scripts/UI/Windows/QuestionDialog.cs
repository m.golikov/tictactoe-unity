using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionDialog : BaseDialogWindow
{
    public Button negativeButton;
    public Image questionImage;

    public override void Initialize<T>(string title, string message, Action<T> action)
    {
        base.Initialize(title, message, action);
        negativeButton.onClick.AddListener(Close);
    }
}
