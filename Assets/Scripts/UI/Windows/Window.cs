using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Window : BaseWindow
{
    public override void Close()
    {
        base.Close();
        gameObject.SetActive(false);
    }

    public override void Open()
    {
        base.Open();
        gameObject.SetActive(true);
    }
}
