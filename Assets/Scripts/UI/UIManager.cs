using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public List<GameObject> states;

    public GameObject activeState 
    {
        get; private set;
    }

    private void Start() 
    {
        foreach(var state in states) {
            if(state.activeSelf) {
                activeState = state;
                break;
            }
        }
    }

    public void ActivateState(string name) 
    {
        foreach(var state in states) {
            state.SetActive(state.name == name);
            if(state.name == name) {
                activeState = state;
            }
        }
    }
}
