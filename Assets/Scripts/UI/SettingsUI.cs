﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsUI : MonoBehaviour
{
    public Settings settings;
    public Text nameText, ratingText;
    public Slider soundVolumeSlider;

    private void Awake() 
    {
        settings.onPlayerNameChanged.AddListener(n => nameText.text = n);
        settings.onPlayerRatingChanged.AddListener(r => ratingText.text = r);
        settings.onSoundVolumeChanged.AddListener(v => soundVolumeSlider.value = v);
    }
}
