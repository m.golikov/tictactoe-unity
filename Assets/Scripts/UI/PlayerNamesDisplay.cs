using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerNamesDisplay : MonoBehaviour
{
    public GameCreator gameCreator;
    public Text currentPlayerText, nextPlayerText, previousPlayerText;

    private void Awake() 
    {
        gameCreator.onGameCreated.AddListener(Initialize);
    }

    public void Initialize(GameData gameData) 
    {
        var gameManager = gameData.field.gameManager;
        gameManager.onTurnChanged.AddListener(p => {
            currentPlayerText.text = p.playerName;
            nextPlayerText.text = gameManager.players[gameManager.GetPlayerIndexByDelta(1)].playerName;
            previousPlayerText.text = gameManager.players[gameManager.GetPlayerIndexByDelta(-1)].playerName;
        });
    }
}
