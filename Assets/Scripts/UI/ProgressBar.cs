using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    public Image fill;
    public Text percentage;

    public virtual void ShowProgress(float progress) 
    {
        fill.fillAmount = progress;
        percentage.text = $"{(int)(progress * 100)}%";
    }
}
