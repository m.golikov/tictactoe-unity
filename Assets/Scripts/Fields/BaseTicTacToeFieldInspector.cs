using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class BaseTicTacToeFieldInspector : MonoBehaviour
{
    public TicTacToeField field;
    public UnityEvent onDraw;
    public Utils.Event<Vector2Int, Vector2Int, int> onWinLineFound = new Utils.Event<Vector2Int, Vector2Int, int>();

    public abstract void CheckField();

    protected virtual void Start() 
    {
        field.onFieldChanged.AddListener((a, b) => {
            CheckField();
        });

        onWinLineFound.AddListener((a, b, c) => {
            field.gameManager.gameIsEnded = true;
        });
    }

    public int GetRequiredLength() 
    {
        var min = Mathf.Min(field.size.x, field.size.y);
        return Mathf.Clamp(Mathf.Min(field.size.x, field.size.y) - (field.gameManager.players.Count - 2), 3, min);
    }
}
