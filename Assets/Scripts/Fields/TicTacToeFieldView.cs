using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TicTacToeFieldView : MonoBehaviour
{
    public TicTacToeField field;
    public RectTransform horizontalLinePrefab, verticalLinePrefab, winLinePrefab;
    public List<RectTransform> markPrefabs;
    public RectTransform fieldParent;
    public float maxYScale = 1.5f;

    private Vector2Int fieldSize;
    private IList<RectTransform> verticalLines = new List<RectTransform>(), horizontalLines = new List<RectTransform>();
    private RectTransform[,] marks;

    private Vector2 cellSize;

    public void InitializeView(Vector2Int fieldSize) 
    {
        fieldParent.sizeDelta = new Vector2(fieldParent.sizeDelta.x, 
                Mathf.Clamp(fieldParent.rect.width * fieldSize.y / fieldSize.x, fieldParent.sizeDelta.x, fieldParent.sizeDelta.x * maxYScale));
        this.fieldSize = fieldSize;

        marks = new RectTransform[fieldSize.x, fieldSize.y];

        cellSize = new Vector2(fieldParent.rect.width / fieldSize.x, fieldParent.rect.height / fieldSize.y);

        CreateLines(verticalLinePrefab, cellSize.x, fieldSize.x, Vector3.right, verticalLines);
        CreateLines(horizontalLinePrefab, cellSize.y, fieldSize.y, Vector3.up, horizontalLines);
    }

    public void ShowMark(Vector2Int coords, int type) 
    {
        if(type == 0) return;
        ShowMarkPrefab(markPrefabs[type - 1], coords);
    }

    public void ShowWinLine(Vector2Int coords, Vector2Int direction, int length) 
    {
        var angle = Vector3.SignedAngle(Vector3.right, direction * cellSize, Vector3.forward);
        var line = Instantiate(winLinePrefab, fieldParent);
        line.localPosition = GetCanvasPosition(coords) - (Vector3)(Vector2)direction * line.sizeDelta.y;
        line.sizeDelta = new Vector2((direction * cellSize).magnitude * (length - 1) + line.sizeDelta.y * 2 * direction.magnitude, line.sizeDelta.y);
        line.localEulerAngles = Vector3.forward * angle;
    }

    public Vector2Int GetFieldCoords(Vector3 fieldPosition) 
    {
        fieldPosition += new Vector3(fieldParent.rect.width * 0.5f, fieldParent.rect.height * 0.5f);
        return new Vector2Int((int)(fieldPosition.x / cellSize.x), (int)(fieldPosition.y / cellSize.y));
    }

    private void ShowMarkPrefab(RectTransform markPrefab, Vector2Int coords) 
    {
        var mark = Instantiate(markPrefab, fieldParent);
        mark.localPosition = GetCanvasPosition(coords);
        mark.sizeDelta = Vector2.one * Mathf.Min(cellSize.x - verticalLinePrefab.rect.width, cellSize.y - horizontalLinePrefab.rect.height);
        marks[coords.x, coords.y] = mark;
    }

    private Vector3 GetCanvasPosition(Vector2Int coords) 
    {
        return new Vector3(fieldParent.rect.width * -0.5f + cellSize.x * (coords.x + 0.5f), fieldParent.rect.height * -0.5f + cellSize.y * (coords.y + 0.5f), 0);
    }

    private void CreateLines(RectTransform linePrefab, float cellSize, int fieldLinearSize, Vector3 direction, IList<RectTransform> container) 
    {
        var basePosition = -cellSize * fieldLinearSize * 0.5f;
        for(int i = 1; i < fieldLinearSize; i++) {
            var line = Instantiate(linePrefab, fieldParent);
            line.localPosition = direction * (basePosition + cellSize * i);
            container.Add(line);
        }
    }
}
