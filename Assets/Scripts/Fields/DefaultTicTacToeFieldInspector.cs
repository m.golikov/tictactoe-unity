using System;
using System.Collections.Generic;
using UnityEngine;

public class DefaultTicTacToeFieldInspector : BaseTicTacToeFieldInspector
{
    public override void CheckField() 
    {
        var requiredLength = GetRequiredLength();

        Vector2Int res, dir;
        dir = Vector2Int.right;
        res = CheckDirectLines(requiredLength, dir);
        if(res != TicTacToeField.INVALID_COORDS) {
            onWinLineFound.Invoke(res, dir, requiredLength);
            return;
        }

        dir = Vector2Int.up;
        res = CheckDirectLines(requiredLength, dir);
        if(res != TicTacToeField.INVALID_COORDS) {
            onWinLineFound.Invoke(res, dir, requiredLength);
            return;
        }

        dir = Vector2Int.one;
        res = CheckDiagonalLines(requiredLength, dir);
        if(res != TicTacToeField.INVALID_COORDS) {
            onWinLineFound.Invoke(res, dir, requiredLength);
            return;
        }

        dir = new Vector2Int(1, -1);
        res = CheckDiagonalLines(requiredLength, dir);
        if(res != TicTacToeField.INVALID_COORDS) {
            onWinLineFound.Invoke(res, dir, requiredLength);
            return;
        }

        CheckDraw(onDraw.Invoke);
    }

    private void CheckDraw(Action onTie) 
    {
        for(int i = 0; i < field.size.x; i++) {
            for(int j = 0; j < field.size.y; j++) {
                if(field.GetCell(i, j) == 0) return;
            }
        }
        onTie();
    }

    private Vector2Int CheckDiagonalLines(int requiredLength, Vector2Int direction) 
    {
        var start = new Vector2Int(direction.x > 0 ? 0 : (field.size.x - 1), direction.y > 0 ? 0 : (field.size.y - 1));
        var end = new Vector2Int(direction.x > 0 ? field.size.x + 1 - requiredLength : requiredLength - 2, 
                direction.y > 0 ? field.size.y + 1 - requiredLength : requiredLength - 2);
        for(int i = start.x; i * direction.x < end.x * direction.x; i += direction.x) {
            for(int j = start.y; j * direction.y < end.y * direction.y; j += direction.y) {
                var win = true;
                
                var diagonalStart = new Vector2Int(i, j);
                for(int k = 0; k < requiredLength - 1; k++) {
                    win &= field.GetCell(diagonalStart + k * direction) != 0 && 
                            field.GetCell(diagonalStart + k * direction) == field.GetCell(diagonalStart + (k + 1) * direction);
                }

                if(win) {
                    return diagonalStart;
                }
            }
        }

        return TicTacToeField.INVALID_COORDS;
    }

    private Vector2Int CheckDirectLines(int requiredLength, Vector2Int direction) 
    {
        var otherDirection = Vector2Int.one - direction;
        for(int i = 0; i <= Utils.Dot(field.size, direction) - requiredLength; i++) {
            for(int j = 0; j < Utils.Dot(field.size, otherDirection); j++) {
                var win = true;
                
                for(int k = 0; k < requiredLength - 1; k++) {
                    win &= field.GetCell((i + k) * direction + j * otherDirection) != 0 && 
                            field.GetCell((i + k) * direction + j * otherDirection) == field.GetCell((i + k + 1) * direction + j * otherDirection);
                }

                if(win) {
                    return i * direction + j * otherDirection;
                }
            }
        }

        return TicTacToeField.INVALID_COORDS;
    }
}
