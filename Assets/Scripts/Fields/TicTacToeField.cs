using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TicTacToeField : MonoBehaviour
{
    public static readonly Vector2Int INVALID_COORDS = -Vector2Int.one;

    public GameManager gameManager;
    public Vector2Int size;
    public bool mutable = true;
    public Utils.Event<Vector2Int, int> onFieldChanged = new Utils.Event<Vector2Int, int>();
    public Utils.Event<Vector2Int> onFieldCreated = new Utils.Event<Vector2Int>();
    public UnityEvent onTryChangeField;

    private int[,] field;

    private void Start() 
    {
        field = new int[size.x, size.y];

        gameManager.onPlayerWon.AddListener(p => mutable = false);

        onFieldCreated.Invoke(size);
    }

    public void SetCell(Vector2Int coords, BasePlayer player) 
    {
        if(mutable && CoordsIsInBounds(coords.x, coords.y) && gameManager.currentPlayer == player && field[coords.x, coords.y] == 0) {
            field[coords.x, coords.y] = gameManager.currentPlayerIndex + 1;
            onFieldChanged.Invoke(coords, gameManager.currentPlayerIndex + 1);
            if(mutable) gameManager.currentPlayerIndex++;
        } else if(mutable) {
            onTryChangeField.Invoke();
        }
    }

    public int GetCell(int x, int y) 
    {
        if(CoordsIsInBounds(x, y)) {
            return field[x, y];
        }

        return 0;
    }

    public int GetCell(Vector2Int coords) 
    {
        return GetCell(coords.x, coords.y);
    }

    private bool CoordsIsInBounds(int x, int y) 
    {
        return field.GetLength(0) > x && field.GetLength(1) > y && x >= 0 && y >= 0;
    }
}
