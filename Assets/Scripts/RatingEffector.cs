using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatingEffector : MonoBehaviour
{
    public Settings settings;
    public GameCreator gameCreator;
    public int ratingPerGame;

    private void Awake() 
    {
        gameCreator.onGameCreated.AddListener(InitializeGameRating);
    }

    public void InitializeGameRating(GameData gameData) 
    {
        gameData.field.gameManager.onPlayerWon.AddListener(player => {
            if(typeof(HumanPlayer).IsInstanceOfType(player) && player.playerName == settings.playerName) {
                settings.rating += ratingPerGame;
            } else if(typeof(AIPlayer).IsInstanceOfType(player)) {
                settings.rating -= ratingPerGame;
            }
        });
    }
}
