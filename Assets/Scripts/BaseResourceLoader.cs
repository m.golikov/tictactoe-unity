using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public abstract class BaseResourceLoader : MonoBehaviour
{
    public Utils.Event<float> onLoadProgress = new Utils.Event<float>();
    public Utils.Event<bool> onLoad = new Utils.Event<bool>();
    public Utils.Event<string> onLoadMessage = new Utils.Event<string>();

    private float _progress;
    public float progress {
        get => _progress;
        set {
            _progress = Mathf.Clamp01(value);
            onLoadProgress.Invoke(_progress);
        }
    }

    public abstract void LoadResources();

    protected virtual void Start() 
    {
        onLoadProgress.AddListener(p => {
            if(p == 1) {
                onLoad.Invoke(false);
            }
        });
        LoadResources();
    }
}
