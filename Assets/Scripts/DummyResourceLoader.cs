using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DummyResourceLoader : BaseResourceLoader
{
    [Serializable]
    public struct LoadMessageData 
    {
        public string message;
        public float progress;
    }

    public float loadTime;
    public int loadTickCount;
    public List<LoadMessageData> loadMessages;

    protected override void Start()
    {
        base.Start();
        onLoadProgress.AddListener(p => {
            Utils.PerformByValue(loadMessages, (a, b) => (int)(b.progress - a.progress), l => l.progress, l => onLoadMessage.Invoke(l.message), p);
        });
    }

    public override void LoadResources()
    {
        progress = 0;
        onLoad.Invoke(true);
        StartCoroutine(DummyLoadCoroutine());
    }

    private IEnumerator DummyLoadCoroutine() 
    {
        for(int i = 0; i < loadTickCount; i++) {
            yield return new WaitForSeconds(loadTime / loadTickCount);
            progress += 1f / loadTickCount;
        }
        progress = 1;
    }
}
