using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    public Settings settings;
    public GameCreator gameCreator;
    public List<AudioSource> sounds;
    public AudioSource markPlacedAudio, markNotPlacedAudio, victoryAudio, failureAudio, drawAudio;

    private IDictionary<AudioSource, float> startVolumes = new Dictionary<AudioSource, float>();

    private void Awake() 
    {
        foreach(var sound in sounds) {
            startVolumes.Add(sound, sound.volume);
        }
        settings.onSoundVolumeChanged.AddListener(ChangeVolume);
        gameCreator.onGameCreated.AddListener(InitializeGameSounds);
    }

    public void ChangeVolume(float value) 
    {
        foreach(var sound in sounds) {
            sound.volume = startVolumes[sound] * value;
        }
    }

    public void InitializeGameSounds(GameData gameData) 
    {
        gameData.field.onFieldChanged.AddListener((a, b) => markPlacedAudio.Play());
        gameData.field.onTryChangeField.AddListener(() => markNotPlacedAudio.Play());
        gameData.fieldInspector.onDraw.AddListener(drawAudio.Play);
        gameData.field.gameManager.onPlayerWon.AddListener(player => {
            if(typeof(HumanPlayer).IsInstanceOfType(player)) {
                victoryAudio.Play();
            } else if(typeof(AIPlayer).IsInstanceOfType(player)) {
                failureAudio.Play();
            }
        });
    }
}
